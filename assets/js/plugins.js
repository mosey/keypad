var counter = 0;
var code = new Array();

function writeNumbers() {
    
    $('.button').bind('click', function() {
            if (counter >= 4) {
                return;
            } else {
                counter ++;
                code.push($(this).attr('data-number'));
                $('.dots').html('');
                $('.dots').append(code);
                //Populates the hidden input field.
                var stringed = parseInt(code.join("").toString());
                $('input[name="code-entered"]').val(stringed);
            }     
    });

    //Cancel Button
    $('.cancel').on('click', function() {
        if (counter <= 0) {
            return
        } else {
        code.pop();
        $('.dots').html('');
        $('.dots').append(code);
        $('input[name="code-entered"]').val(parseInt(code.join("").toString()));
        counter -= 1;            
        }
    });
}

function keyPresses() {
    $('body').bind('keydown', function(e) {
        //console.log(e.keyCode);

        switch(e.keyCode) {
            case 8:
                e.preventDefault();
                $('.cancel').trigger('click');
                break;

            case 13:
                e.preventDefault();
                $('input.submit').trigger('click');
                break;

            case 48:
            case 96:
                $('.button[data-number="0"]').trigger('click');
                break;    

            case 49:
            case 97:
                $('.button[data-number="1"]').trigger('click');
                break;

            case 50:
            case 98:
                $('.button[data-number="2"]').trigger('click');
                break;

            case 51:
            case 99:
                $('.button[data-number="3"]').trigger('click');
                break;

            case 52:
            case 100:
                $('.button[data-number="4"]').trigger('click');
                break;    

            case 53:
            case 101:
                $('.button[data-number="5"]').trigger('click');
                break;

            case 54:
            case 102:
                $('.button[data-number="6"]').trigger('click');
                break;

            case 55:
            case 103:
                $('.button[data-number="7"]').trigger('click');
                break;

            case 56:
            case 104:
                $('.button[data-number="8"]').trigger('click');
                break;

            case 57:
            case 105:
                $('.button[data-number="9"]').trigger('click');
                break;  

            case 27:
                $('span.cover-close').trigger('click');
                break;    
        }       

    });
}

function formSubmit() {
    $('form').submit(function(e) {
        e.preventDefault();
        if($('input[name=code-entered]').val().length < 4) {
            alert('You must enter four numbers to continue');
            return false;
        } else {
            $('form').find('input[type=submit]').attr('disabled','disabled');

            var formData = $('form').serialize();
           
            $.ajax({
                type: 'POST',
                url: $('form').attr('action'),
                data: formData,
                dataType: 'json',
                encode: true
            }).done(function(data) {
                $('#cover-content').addClass(data.class);
                $('#cover-content').append('<h3>' + data.result + '</h3>')
                $('#cover-content').append('<h4>' + data.direction + '</h4>')
                $('.cover').addClass('open');
                if (data.img) {
                    $('#cover-content').append('<img src="assets/img/' + data.img +'" class="success-image" />')
                }
            });
            $('span.cover-close').click(function() {
                resetAll();
            })
        }
    })

}

function resetAll() {
    $('input[name=code-entered').val('');
    $('.dots').html('');
    $('.cover').removeClass('open');
    $('#cover-content').html('');
    $('form').find('input[type=submit]').removeAttr('disabled','disabled');
    $('#cover-content').removeAttr('class');
    counter = 0;
    code = [];
}

function touchHighlight() {
    $('.button').bind('click', function() {
        $(this).addClass('activated');
        setTimeout(function() {
            $('.button').removeClass('activated');
        }, 500);
    })
}

function checkInputs() {
    if($('input[name=code-entered]').val().length < 4) {
        alert('You must enter four numbers to continue');
        return false;
    }
}

$(document).ready(function() {
    touchHighlight();
    writeNumbers();
    keyPresses();
    formSubmit();

});
