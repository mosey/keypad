
<?php 

	if($_SERVER["REQUEST_METHOD"] == "POST") {
		
		$code = $_POST['code-entered'];
		$theNumber = 5098;
		$data = array();

		$ltArray = 
			Array (
				"Way too low, go on, go again. . . but maybe aim a bit higher",
				"You are very low. . . if I was you I'd go for a bit of a higher number",
				"You are low.  I  wouldn't have thought you could have gotten any lower but dear god!",
				"Higher. . . just go higher. . . but not too much higher.  What do I know?  I'm just a computer."
			);

		$gtArray = 
			Array (
				"Dude...you're high! ....Maybe we're all a little bit high, but you most definitely are high.",
				"Airplanes don't go as high as you.  Maybe you should try and reduce that number a little bit more",
				"You're too high.  It's like you're in space or something.  Drop the ball a bit... and your number",
				"If you were a juggling siteswap, you'd be Z.  If you don't know siteswap, it means you're very high."
			);	



		if($code < $theNumber) {
			$data['result'] = $ltArray[array_rand($ltArray)];
			$data['direction'] = "Go Higher!";
			$data['class'] = "low";
		} elseif ($code > $theNumber) {
			$data['result'] = $gtArray[array_rand($gtArray)];
			$data['direction'] = "Go Lower!";
			$data['class'] = "high";
		} elseif ($code == $theNumber) {
			$data['result'] = "You made it.";
			$data['direction'] = "Here's your reward.";
			$data['img'] = "tom-selleck.jpg";
			$data['class'] = "correct";
		}

		echo json_encode($data);
	}

 ?>